import boto3
from botocore.client import Config

import csv

import os

import json

from PIL import Image

import random
import requests

import shutil
import stripe
import sys

from dotenv import load_dotenv
load_dotenv(dotenv_path=sys.path[0] + '/.env')

valid_letters = 'ABCDFGHJKMPRTVWXY'

# Input stream for reading in an optional file of all items.
input_stream = open(sys.argv[1], 'r', encoding='utf-8-sig')

# Product identifier for Stripe product.
cat_pillow_product = 'prod_FUje0AXbk5Mj42'
cat_pillow_product_test = 'prod_Fw1dwVRvnr1Dpl'

# Table from airtable
airtable_url = "https://api.airtable.com/v0/appfNsSLA1UinTZTg/vet_list"

# Stripe public and private keys.
pk_test = os.getenv('pk_test')
sk_test = os.getenv('sk_test')
pk_live = os.getenv('pk_live')
sk_live = os.getenv('sk_live')

# Airtable Key
airtable_key = os.getenv('airtable_key')

# DigitalOcean storage keys.
spaces_pk = os.getenv('spaces_pk')
spaces_sk = os.getenv('spaces_sk')



def upload_image(logo):
  session = boto3.session.Session()
  client = session.client('s3',
                        region_name='nyc3',
                        endpoint_url='https://worldsproject.nyc3.digitaloceanspaces.com',
                        aws_access_key_id=spaces_pk,
                        aws_secret_access_key=spaces_sk)
  try:
      random_key = random.choices(valid_letters, k=20)
      client.upload_file(logo, 'vet_images', 'image_{}.jpg'.format(random_key))
      logo = client.generate_presigned_url(ClientMethod='get_object', 
                                    Params={'Bucket': 'vet_images',
                                            'Key': 'image_{}.jpg'.format(random_key)})
      return logo
  except:
      pass

def pillow_skus(vet, quantity, price, image, live):
    if live:
        product_sku = cat_pillow_product
    else:
        product_sku = cat_pillow_product_test

    pillow = stripe.SKU.create(
      product=product_sku,
      attributes={
        "name": vet
      },
      image=upload_image(image),
      metadata={'quantity':quantity},
      price=price*100,
      currency="usd",
      inventory={"type": "infinite"}
    )
   
    return pillow

def add_vet_to_airtable(vet_name, address, identifier, phone_number, url, city, state, zipcode, mailed, live):
  headers= {"authorization": "Bearer keyPIVbjBEXJ8wt3H",
            'content-type': 'application/json'}
  data = {"fields": {
          "Vet Name": vet_name,
          "Street Address": address,
          "Custom URL": identifier,
          "Phone": phone_number,
          "Web Address": url,
          "City": city,
          "State": state,
          "Zipcode": zipcode,
          "Sample Mailed": mailed,
          "Test Item": live
          }
        }
  x = requests.post(airtable_url, headers=headers, data=json.dumps(data))

csv_reader = csv.DictReader(input_stream)
for line in csv_reader:
    # Generating the unique identifier
    identifier = ''.join(random.choices(valid_letters, k=6))

    while os.path.exists(identifier+'.html'):
        identifier = ''.join(random.choices(valid_letters, k=6))

    html = sys.path[0] + '/' + identifier + '.html'

    # Grabbing the information from the csv file.
    vet_name = line['Vet Name']
    address = line['Street Address']
    city = line['City']
    state = line['State']
    zipcode = line['Zipcode']
    phone_number = line['Phone']
    url = line['Web Address']
    image_path = line['Promo Image']
    mailed = line['Sample Mailed']
    live = (len(sys.argv) > 2 and sys.argv[2] == 'live')

    if mailed == 'y':
        mailed = True
    else:
        mailed = False
      
    if live:
        stripe_key = sk_live
        stripe_pk = pk_live
    else:
        stripe_key = sk_test
        stripe_pk = pk_test
      
    stripe.api_key = stripe_key
    fifty = pillow_skus(vet_name, 50, 100, image_path + '_50.jpg', live)['id']
    hundred = pillow_skus(vet_name, 100, 180, image_path + '_100.jpg', live)['id']
    add_vet_to_airtable(vet_name, address, identifier, phone_number, url, city, state, zipcode, mailed, not live)

    with open(sys.path[0] + '/test.html') as f:
        newText = f.read()
        newText = newText.replace('{{ VET_NAME }}', vet_name)
        newText = newText.replace('{{ 50 IMG }}', image_path + '_50.jpg')
        newText = newText.replace('{{ 100 IMG }}', image_path + '_100.jpg')
        newText = newText.replace('{{ 50 STRIPE }}', 'checkout-button-' + fifty)
        newText = newText.replace('{{ 100 STRIPE }}', 'checkout-button-' + hundred)
        newText = newText.replace('{{ 50 STRIPE SKU }}', fifty)
        newText = newText.replace('{{ 100 STRIPE SKU }}', hundred)
        newText = newText.replace('{{ FORM NAME }}', vet_name)
        newText = newText.replace('{{ STRIPE KEY }}', stripe_pk)

    with open(html, "w") as f:
        f.write(newText)

    # Copying images to directory for netlify.
    size = 400,400
    im = Image.open(image_path + '_50.jpg')
    im.thumbnail(size)
    im.save(sys.argv[0][:-10] + 'img/thumbs/' + image_path + '_50.jpg', 'jpeg')
    im = Image.open(image_path + '_100.jpg')
    im.thumbnail(size)
    im.save(sys.argv[0][:-10] + 'img/thumbs/' + image_path + '_100.jpg', 'jpeg')
